---
menu:
  main:
    name: Kontact
    params:
      components:
      - background: /assets/img/kontact-menu-features.png
        large: true
        name: Característiques del Kontact
        url: /components/kontact
      - background: /assets/img/icon-kmail.svg
        name: KMail
        url: /components/kmail
      - background: /assets/img/icon-korganizer.svg
        name: KOrganizer
        url: /components/korganizer
      - background: /assets/img/icon-kaddressbook.svg
        name: KAddressBook
        url: /components/kaddressbook
      - background: /assets/img/icon-akregator.svg
        name: Akregator
        url: /components/akregator
      - background: /assets/img/icon-knotes.svg
        name: KNotes
        url: /components/knotes
      - background: /assets/img/icon-akonadi.png
        name: Akonadi
        url: /components/akonadi
    weight: 1
title: Paquet del Kontact - La potent solució de PIM
---
