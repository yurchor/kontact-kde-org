---
appstream: org.kde.kontact
components: true
flatpak: true
flatpak_exp: https://community.kde.org/KDE_PIM/Flatpak
gear: true
konqi: /assets/img/konqi-mail.png
layout: download
menu:
  main:
    weight: 2
name: Kontact
sources:
- description: 'El Kontact s''estén per una multitud de repositoris. Vegeu la nostra
    pàgina [com participar](/get-involved) per obtenir instruccions detallades sobre
    com obtenir tots els repositoris del Kontact.

    '
  name: Git
  src_icon: /reusable-assets/git.svg
- description: 'Actualment estem treballant per adaptar el Kontact al Windows. Voleu
    ajudar-nos? Vegeu [com participar](/get-involved) i poseu-vos en contacte amb
    nosaltres!

    '
  name: Windows
  src_icon: /reusable-assets/windows.svg
title: Baixada
---
