---
getintouch: Els desenvolupadors de Kontact generalment es troben pels canals d'IRC
  [#kontact](irc://irc.libera.chat/kontact) i [#akonadi](irc://irc.libera.chat/akonadi)
  en el xat de Libera. La majoria dels debats relacionats amb el desenvolupament tenen
  lloc a la [llista de correu kde-pim](https://mail.kde.org/mailman/listinfo/kde-pim/).
  Uniu-vos, digueu hola i expliqueu-nos en què us agradaria ajudar-nos!
layout: get-involved
menu:
  main:
    weight: 4
name: Kontact
title: Col·laboreu-hi
userbase: Kontact
---
El codi font del Kontact s'estén a través de molts components en molts repositoris: és fàcil perdre's i aclaparar-se si sou nou. Si voleu involucrar-vos en el desenvolupament del Kontact simplement llegiu els materials enllaçats des d'aquesta pàgina i poseu-vos en contacte amb nosaltres: us ajudarem a trobar el camí pel nostre codi base.

**Comenceu llegint informació sobre el [wiki de la comunitat KDE PIM](https://community.kde.org/KDEPIM).**

## Oportunitats de feina

Cerqueu algunes tasques senzilles per començar a millorar el Kontact? Llavors revises les nostres [oportunitats de feina](https://phabricator.kde.org/tag/kde_pim_junior_jobs/). Hem recollit una sèrie de tasques que són bastant senzilles i aïllades i no requereixen una comprensió en profunditat de tota la base de codis i l'arquitectura, un punt d'entrada perfecte en el món del Kontact.

Si voleu començar a treballar en alguna tasca, us la podeu assignar a vosaltres mateixos (a menys que ja estigui assignada a algú altre) i feu-nos-ho saber en els comentaris de sota la tasca.
