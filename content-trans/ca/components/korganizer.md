---
description: 'El KOrganizer és un organitzador potent ple de característiques, gestor
  d''esdeveniments i tasques pendents que s''integra bé amb la resta del Kontact,
  especialment amb el KMail.

  '
layout: component
screenshot: /assets/img/kontact-korganizer.png
shortDescription: 'Organitzeu el vostre temps! Gestioneu esdeveniments, tasques pendents,
  cites i més.

  '
title: KOrganizer
weight: 30
---
El KOrganizer és el component de calendari i planificació del Kontact. Proporciona la gestió d'esdeveniments i tasques, notificació d'alarmes, exportació web, gestió de xarxa transparent de les dades, planificació de grups, la importació i l'exportació de fitxers de calendari i més. És capaç de treballar juntament amb una àmplia varietat de serveis de calendari, incloent-hi NextCloud, Kolab, Google Calendar i altres. El KOrganizer és completament personalitzable a les vostres necessitats i és una part integral del paquet Kontact, que té com a objectiu ser una solució completa per organitzar les vostres dades personals. El KOrganizer accepta els dos estàndards dominants per emmagatzemar i intercanviar dades de calendari, vCalendar i iCalendar.

## Característiques

* **Suport per a calendaris múltiples i llistes de tasques pendents.** El Korganizer pot fusionar de manera transparent dades de calendari des de diferents fitxers o altres fonts de dades de calendari, per exemple calendaris a la web. Es poden activar, desactivar, afegir i eliminar còmodament des de la interfície gràfica d'usuari.
* **Integració del Kontact.** El KOrganizer està completament integrat amb el Kontact, l'aplicació completa de gestió de la informació personal. Dins del Kontact hi ha algunes característiques addicionals disponibles com ara la conversió de correus a esdeveniments o tasques pendents mitjançant arrossegar i deixar anar.
* **Model d'emmagatzematge.** El KOrganizer té un calendari persistent. No cal que l'usuari tingui cura de carregar o desar el calendari. Els canvis es desen immediatament al disc. Si el calendari es canvia externament, es carrega i s'actualitza automàticament a la vista. Un mecanisme de bloqueig gestiona l'accés concurrent al calendari.
* **Desfer i refer.** El KOrganizer permet desfer i refer sense límit.
* **Integració de les tasques pendents amb la vista d'agenda.** Les tasques pendents es mostren a les vistes setmanals i diàries. Les tasques pendents es poden convertir en esdeveniments arrossegant-les des de la llista de tasques pendents i deixant-les anar a la vista d'agenda.
* **Adjunts per esdeveniments i tasques pendents.** Les referències a pàgines web, fitxers locals o correus es poden adjuntar a esdeveniments i tasques pendents. Les dades adjuntes es poden accedir fàcilment amb un sol clic des de la vista d'esdeveniments o tasques pendents així com la vista de resum o els editors
* **Introducció ràpida de tasques pendents.** Un camp d'entrada especial permet crear ràpidament una tasca pendent sense necessitat d'obrir cap editor. Això és especialment pràctic per crear múltiples tasques pendents a la vegada.
* **Introducció ràpida d'esdeveniments.** Hi ha diverses maneres de crear esdeveniments des de la vista d'agenda: els esdeveniments per tecleig avançat es poden crear seleccionant un interval de temps i després simplement començant a escriure. S'obrirà un editor i el text teclejat s'inclourà al títol. Opcionalment l'editor d'esdeveniments es pot obrir quan ha acabat la selecció d'hores, i a més de les entrades habituals del menú i de la barra d'eines hi ha vinculacions de tecles i un menú contextual per iniciar el diàleg de l'editor.
* **Permet la impressió.** Els calendaris es poden imprimir utilitzant diversos estils diferents. La impressió també admet colors i esdeveniments superposats.
* **Integració del KMail.** El KMail permet la transferència directa d'invitacions i altres adjunts de calendari al KOrganizer.
