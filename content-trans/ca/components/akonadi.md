---
description: ''
layout: component
screenshot: /assets/img/akonadi.png
shortDescription: ''
title: Akonadi
weight: 70
---
El marc de treball Akonadi, anomenat així per la deessa oracle de la justícia a Ghana, és responsable de proporcionar aplicacions amb una base de dades centralitzada per emmagatzemar, indexar i recuperar la informació personal de l'usuari. Això inclou els correus electrònics, contactes, calendaris, esdeveniments, revistes, alarmes, notes, etc. de l'usuari.

## Característiques

* Memòria cau de dades comunes de PIM
* Tipus de disseny agnòstic
* Ampliabilitat
* Accés genèric fora de línia, canvi d'enregistrament i repetició
* Detecció i resolució genèrica de conflictes
* Els recursos es poden agrupar per perfil
* Elements composts de múltiples parts recuperables independentment
* És possible la recuperació de còpia zero
* L'accés concurrent permet l'activitat en segon pla independent de la IU del client
* Sincronització del correu, el calendari, les llibretes d'adreces a servidors remots
* Permet que la infraestructura semàntica d'escriptori accedeixi a les dades PIM
* Arxivament
* Indexació
* Cerca fora de procés
* Disseny multiprocés
* Aïllament de fallades
* Els elements grans no poden blocar tot el sistema
* Enllaç per IPC permet components propietaris
