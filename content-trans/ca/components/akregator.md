---
description: 'L''Akregator és un lector de canals RSS. Pot descarregar automàticament
  els canals RSS dels llocs preferits i permet llegir els articles fins i tot quan
  esteu fora de línia. També proporciona una funcionalitat de cerca avançada i característiques
  d''arxiu.

  '
layout: component
screenshot: /assets/img/kontact-akregator.png
shortDescription: Estigueu a sobre de les notícies amb el nostre lector d'RSS.
title: Akregator
weight: 40
---
L'Akregator ajuda a mantenir-vos informats sobre històries noves en llocs web com ara el KDE Dot News i els blogs del Planet KDE. La tecnologia usada és RSS i molts llocs la implementen.

## Característiques

* Senzill d'utilitzar
* Pot acceptar dotzenes de canals
* Pot notificar-vos els canals no llegits
* Utilitza les pestanyes per donar accés a la lectura interna de les històries completes
* Arxiu de canals
* Importa i exporta canals
