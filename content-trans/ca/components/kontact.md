---
description: 'El Kontact unifica totes les nostres aplicacions PIM en una finestra
  única per donar-vos la millor experiència possible i impulsar la vostra productivitat.
  Amb el Kontact podeu utilitzar totes les eines del nostre potent paquet en un sol
  lloc. A diferència de les aplicacions web propietàries, valorem la vostra privadesa
  i admetem estàndards oberts, i és per això que el Kontact us dona un control total
  de les vostres dades. Per descomptat també podeu accedir a les vostres dades fora
  de línia si voleu.

  '
layout: component
screenshot: /assets/img/kontact-summary.png
shortDescription: 'Gestioneu el correu electrònic, calendaris i contactes des d''una
  única finestra.

  '
title: Kontact
weight: 10
---
El Kontact és un paquet de gestió d'informació personal ple de característiques creat per [KDE](https://kde.org). El Kontact unifica totes les nostres aplicacions PIM en una sola finestra de manera que es puguin tractar els vostres correus electrònics, calendaris, notícies i altres dades de forma encara més eficient. També podeu executar cadascun dels components del Kontact com una aplicació independent.

## Ple de característiques

Creiem que el programari hauria d'adaptar-se als vostres fluxos de treball, en lloc d'obligar-vos a adaptar-vos als seus. El Kontact és molt configurable i personalitzable perquè pugueu configurar-lo exactament de manera que treballi per a vós. Consulteu les pàgines individuals dels components del Kontact per obtenir una visió general de les característiques que poden oferir cadascun d'ells.

## Flexible

El Kontact és compatible amb una gran varietat de serveis i protocols de correu electrònic, calendari i llibreta d'adreces i es pot ampliar fàcilment per donar suport a serveis addicionals. No cal que canvieu el vostre proveïdor de correu electrònic o migreu el vostre calendari a un altre lloc només per sincronitzar-lo al Kontact. Feu que el Kontact apunti al servei on estan les vostres dades PIM i el Kontact les mostrarà i les mantindrà sincronitzades.

El Kontact també pot importar les vostres dades PIM i alguns paràmetres d'altres paquets PIM com ara el Thunderbird i l'Evolution.

## Seguretat

El Kontact implementa tots els estàndards de seguretat moderns per protegir la vostra privadesa. Totes les comunicacions amb servidors remots estan encriptades per defecte utilitzant l'encriptatge SSL/TLS estàndard de la indústria. El Kontact també té un suport integrat per signar i xifrar correus electrònics amb GPG i S/MIME i pot verificar automàticament les signatures dels correus electrònics.

El Kontact està configurat de manera predeterminada per protegir la vostra privadesa bloquejant la càrrega de contingut extern, respostes automàtiques i altres trucs utilitzats sovint pels estafadors.

## Integrat

Tots els components del Kontact estan ben integrats. El KOrganizer pot enviar invitacions de reunió a través de comptes de correu electrònic configurats al KMail. El KMail lliurarà les invitacions de reunió i us mostrarà els conflictes amb esdeveniments dels vostres calendaris. Podeu barrejar les vostres tasques pendents i esdeveniments normals a la vista d'agenda del KOrganizer.

## Codi font lliure i obert

El Kontact és lliure (tant en el sentit de la llibertat d'expressió com de la cervesa de franc) i de codi completament obert. Això significa que el [codi font](https://invent.kde.org/pim/kontact) és públic perquè tothom el pugui veure, auditar i millorar.

Estem orgullosos de la nostra obertura i de l'enfocament en la protecció de la privadesa dels usuaris. El Kontact no us espiarà, no mostrarà cap anunci, no emmagatzemarà les vostres dades confidencials a qualsevol lloc d'un núvol propietari o les compartirà amb ningú més. Vegeu la [Política de privadesa del programari KDE](https://kde.org/privacypolicy-apps/) per a més detalls.
