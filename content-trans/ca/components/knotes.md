---
description: ''
layout: component
shortDescription: ''
title: KNotes
weight: 50
---
El KNotes és un programa que permet escriure l'equivalent informàtic de les notes adhesives. Les notes es guarden automàticament quan sortiu del programa, i es mostren quan obriu el programa.

## Característiques

* Escriu les notes segons la vostra elecció de tipus de lletra i color de fons
* Utilitzeu arrossegar i deixar anar per enviar les vostres notes
* Es poden arrossegar al calendari per reservar un interval de temps
* Les notes es poden imprimir
