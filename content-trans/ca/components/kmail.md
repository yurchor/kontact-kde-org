---
description: 'El KMail és un client de correu electrònic que s''integra bé amb els
  proveïdors de correu electrònic àmpliament utilitzats com el GMail. Proporciona
  moltes eines i característiques per maximitzar la vostra productivitat i fa que
  treballar amb comptes grans de correu electrònic sigui fàcil i ràpid. El KMail permet
  una gran varietat de protocols de correu electrònic: POP3, IMAP, Microsoft Exchange
  (EWS) i més.

  '
layout: component
screenshot: /assets/img/kontact-kmail.png
shortDescription: Potencieu la vostra productivitat amb una aplicació de correu electrònic
  plena de característiques.
title: KMail
weight: 20
---
El KMail és el component de correu electrònic del Kontact, el gestor integrat d'informació personal de KDE.

## Seguretat

* **Segur de manera predeterminada.** Configuració predeterminada amb el compromís en la seguretat i privadesa en lloc de les característiques.
* **Encriptatge d'extrem a extrem.** El suport per a OpenPGP i S/MIME està integrat, i un assistent de configuració i generació de claus ajuda els usuaris a començar amb això.
* **Encriptatge de transports.** La comunicació encriptada SSL/TLS està implementada per descomptat, així com una sèrie de mètodes d'autenticació com el GSSAPI (Kerberos) o l'OAuth2.
* **Aïllament del contingut HTML.** Si no es poden evitar els correus electrònics HTML, el KMail assegura que no hi ha referències externes que puguin ocultar-se per filtrar informació o comprometre la vostra privadesa. Addicionalment, les proteccions de pesca del KMail adverteixen sobre els enllaços sospitosos en aquests correus electrònics.
* **Protecció de correu brossa.** Si el servidor de correu electrònic ja no s'encarrega d'això, el KMail pot integrar localment verificadors de correu brossa populars com ara SpamAssassin o Bogofilter.

## Potent

* **Capacitats fora de línia.** El KMail permet operar sense connectivitat de xarxa sincronitzant (opcionalment) tot el vostre correu electrònic sencer localment.
* **Identitats múltiples.** El KMail separa les identitats dels comptes, donant-vos una gran flexibilitat quan porteu barrets diferents en diverses organitzacions.
* **Plantilles.** Un sistema flexible de plantilles i fragments de text permet automatitzar parts de l'escriptura de correus electrònics.
* **Multiidioma.** Suport integrat de traducció del contingut, i verificació ortogràfica per paràgraf, amb detecció automàtica de l'idioma quan es tracta de contingut en diversos idiomes.
* **Redactor productiu.** El redactor de correu electrònic del KMail ve amb molts ajudants petits per fer-vos més eficient, com ara l'avís sobre els adjunts oblidats, la compressió d'adjunts en línia o el canvi de mida de les imatges adjuntes.
* **Filtratge potent.** A més d'automatitzar l'ordenació dels correus per una multitud de condicions, el sistema de filtratge del KMail també es pot utilitzar per implementar fluxos de treball arbitraris ja que pot integrar-se amb aplicacions externes.
* **Cerca i etiquetatge.** El marcatge i etiquetatge dels missatges ajuda a ordenar i recuperar informació, i també ho fan les potents capacitats de cerca local i remota.
* **Gestió de llistes de correu.** Les llistes de correu del Mailman es poden detectar automàticament i les operacions de gestió de les llistes de correu estan directament disponibles en el KMail.
* **Configuració flexible.** Les incomptables opcions de configuració permeten ajustar molts aspectes del KMail a les vostres necessitats i desitjos.

## Integrat

* **Invitacions d'iTip.** Els correus d'invitació a reunions es detecten i ofereixen respostes ràpides en el lloc, i per descomptat s'envien al [KOrganizer](../korganizer).
* **Llibreta d'adreces.** A més de la compleció automàtica d'adreces del [KAddressBook](../kaddressbook), el KMail també utilitza avatars i preferències criptogràfiques des d'allà.
* **Correus de reserves.** El KMail pot detectar reserves de vol o hotel en els correus electrònics de determinats proveïdors i ofereix afegir-los al vostre calendari.

## Compliment dels estàndards

* Implementa els protocols de correu estàndard IMAP, POP3 i SMTP.
* Implementa el correu electrònic «push» (IMAP IDLE).
* Permet totalment la cerca a les carpetes IMAP.
* Suport natiu per a l'OpenPGP en línia, PGP/MIME, i S/MIME.
* Implementa el filtratge Sieve a la banda del servidor.
