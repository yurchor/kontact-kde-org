---
description: 'El KAddressBook us permet gestionar els vostres contactes fàcilment.
  Està profundament integrat amb la resta del Kontact, permetent-vos triar fàcilment
  els vostres contactes quan envieu un correu electrònic o creant una invitació a
  una reunió.

  '
layout: component
screenshot: /assets/img/kontact-kaddressbook.png
shortDescription: Gestioneu els contactes fàcilment.
title: KAddressBook
weight: 30
---
El KAddressBook emmagatzema tots els detalls personals de la vostra família, amistats i altres contactes. Permet una gran varietat de serveis, incloent-hi NextCloud, Kolab, Google Contacts, Microsoft Exchange (EWS) o qualsevol servidor estàndard de CalDAV.

## Característiques

* **Cerca potent.** El KAddresbook té filtres configurables i capacitats de cerca potent.
* **Detecció de duplicats i fusió.** El KAdressbook pot trobar contactes duplicats de múltiples fonts i els pot fusionar en un contacte únic.
* **Integració.** S'integra amb altres components del Kontact, p. ex., exportant recordatoris del dia de naixement al [KOrganizer](../korganizer).
* **Codis QR.** El KAddressbook pot mostrar un codi QR per a cada contacte per escanejar-lo ràpidament al telèfon o enviar-lo a algú.
* **Administració estàndard avantatjosa.** El KAddresbook pot importar i exportar contactes des de i cap a gairebé tots els estàndards de llibreta d'adreces.
* **Integració LDAP.** Es pot connectar a diversos servidors LDAP, que després es poden utilitzar per a la compleció automàtica de contactes quan es redacten correus electrònics al [KMail](../kmail).
