---
description: 'Akregator is an RSS feed reader. It can automatically download RSS feeds
  from your favorite sites and allows you to read the articles even when you are offline.
  Also provides rich search functionality and archiving features.

  '
layout: component
screenshot: /assets/img/kontact-akregator.png
shortDescription: Ostanite na tekočem z novicami z našim bralnikom RSS.
title: Akregator
weight: 40
---
Akregator vam pomaga, da ste obveščeni o novih zgodbah na spletnih mestih, kot sta KDE Dot News in blogih Planet KDE. Uporabljena tehnologija je RSS in številna spletna mesta jo podpirajo.

## Odlike

* Preprosta uporaba
* Lahko sprejme na desetine virov
* Lahko vas obvesti o neprebranih virih
* Uporablja zavihke za dostop do notranjega branja celotnih zgodb
* Arhiv virov
* Uvoz in izvoz virov
