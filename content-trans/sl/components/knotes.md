---
description: ''
layout: component
shortDescription: ''
title: KNotes
weight: 50
---
KNotes is a program that lets you write the computer equivalent of sticky notes. The notes are saved automatically when you exit the program, and they display when you open the program.

## Odlike

* Zapišite opombe v pisavi in barvi ozadja po svojem okusu
* Uporabite povleci in spusti za pošiljanje zapiskov po e-pošti
* Lahko jih povlečete v koledar, da rezervirate termin
* Opombe je mogoče natisniti
