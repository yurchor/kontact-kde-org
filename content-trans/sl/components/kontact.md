---
description: 'Kontact združuje vse naše aplikacije PIM v eno okno, da vam zagotovi
  najboljšo možno izkušnjo in poveča vašo produktivnost. S Kontactom lahko na enem
  mestu uporabljate vsa orodja našega zmogljivega paketa. Za razliko od lastniških
  spletnih aplikacij cenimo vašo zasebnost in podpiramo odprte standarde, zato vam
  Kontact omogoča popoln nadzor nad vašimi podatki. Seveda lahko do svojih podatkov
  dostopate tudi brez povezave, če želite.


  '
layout: component
screenshot: /assets/img/kontact-summary.png
shortDescription: 'Upravljajte z vsemi e-poštnimi sporočili, koledarji in stiki v
  enem samem oknu.

  '
title: Kontact
weight: 10
---
Kontact is a feature-rich personal information management suite created by [KDE](https://kde.org). Kontact unifies all our PIM applications into a single window so that you can deal with your emails, calendaring, news and other data even more efficiently. You can also run each of the components of Kontact as a standalone application.

## Bogat s funkcijami

We believe that software should adapt to your workflows, instead of forcing you to adapt to theirs. Kontact is highly configurable and customizable so that you can set it up exactly the way that works for you. Check out the individual pages for Kontact's components to get an overview of some of the features each of them can offer.

## Prilagodljivo

Kontact podpira veliko različnih storitev in protokolov za e-pošto, koledarje in adresarje ter ga je mogoče enostavno razširiti za podporo dodatnih storitev. Ni vam treba spremeniti ponudnika e-pošte ali preseliti svojega koledarja drugam, samo da bi ga sinhronizirali s Kontactom. Preprosto usmerite Kontact na storitev, kjer so vaši podatki PIM, in Kontact jih bo prikazal in ohranil sinhronizacijo.

Kontact can also import your PIM data and some settings from other PIM suites like Thunderbird and Evolution.

## Varno

Kontact supports all modern security standards to protect your privacy. All communication with remote servers is encrypted by default using the industry standard SSL/TLS encryption. Kontact also has a built-in support for signing and encryption emails with GPG and S/MIME and it can automatically verify emails signatures.

Kontact is configured by default to protect your privacy by blocking loading external content, automatic responses and other tricks often used by scammers.

## Integrated

All components of Kontact are well integrated together. KOrganizer can send meeting invitations through email accounts configured in KMail. KMail will render meeting invitations and show you conflicts with events in your calendars. You can mix your Todos and regular events in KOrganizer's Agenda view.

## Brezplačno in odprto-kodno

Kontact is free (both as in free speech and free beer) and completely open source. It means that the [source code](https://invent.kde.org/pim/kontact) is out there for anyone to see, audit and improve.

We pride ourselves in our openess and our focus on protecting users' privacy. Kontact will not spy on you, won't show any advertisment, it will not store your sensitive data somewhere in a proprietary cloud or share them with anyone else. See the [KDE Software Privacy Policy](https://kde.org/privacypolicy-apps/) for more details.
