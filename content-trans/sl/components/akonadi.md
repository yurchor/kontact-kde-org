---
description: ''
layout: component
screenshot: /assets/img/akonadi.png
shortDescription: ''
title: Akonadi
weight: 70
---
The Akonadi framework, named after the oracle goddess of justice in Ghana is responsible for providing applications with a centralized database to store, index and retrieve the user's personal information. This includes the user's emails, contacts, calendars, events, journals, alarms, notes, etc.

## Odlike

* Common PIM data cache
* Type agnostic design
* Razširljivost
* Splošni dostop brez povezave, sprememba snemanja in ponovitev
* Splošno odkrivanje in razreševanje sporov
* Vire je mogoče združiti po profilu
* Predmeti, sestavljeni iz več delov, ki jih je mogoče samostojno pridobiti
* Možno je pridobivanje brez kopije
* Concurrent access allows background activity independent of UI client
* Syncing mail, calendar, addressbooks to remote servers
* Omogoča semantični namizni infrastrukturi dostop do podatkov PIM
* Arhiviranje
* Indeksiranje
* Iskanje izven procesa
* Večprocesna zasnova
* Izolacija sesutja
* Veliki predmeti ne morejo blokirati celotnega sistema
* Povezava z IPC omogoča lastniške komponente
