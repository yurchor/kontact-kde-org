---
getintouch: The Kontact developers generally hang around on the [#kontact](irc://irc.libera.chat/kontact)
  and [#akonadi](irc://irc.libera.chat/akonadi) IRC channels on Libera Chat. Most
  development-related discussions take place on the [kde-pim mailing list](https://mail.kde.org/mailman/listinfo/kde-pim/).
  Just join in, say hi and tell us what you would like to help us with!
layout: get-involved
menu:
  main:
    weight: 4
name: Kontact
title: Sodelujte
userbase: Kontact
---
Kontact's source code is spread across many components in many repositories - it is easy to get lost and overwhelmed if you are new to it. If you want to get involved in development of Kontact just read the materials linked from this page and get in touch with us - we will help you find your way around our codebase.

**Please start by reading some information on the [KDE PIM Community wiki](https://community.kde.org/KDE_PIM).**

## Junior Jobs

Looking for some simple tasks to get started with improving Kontact? Then check out our [junior jobs](https://phabricator.kde.org/tag/kde_pim_junior_jobs/). We have collected a number of tasks that are fairly simple and isolated and don't require in-depth understanding of the entire codebase and architecture - perfect entry point into the beautiful world of Kontact.

If you would like to start working on some task, feel free to assign it to yourself (unless it's already assigned to someone else) and let us know in the comments below the task.
