---
appstream: org.kde.kontact
components: true
flatpak: true
flatpak_exp: https://community.kde.org/KDE_PIM/Flatpak
gear: true
konqi: /assets/img/konqi-mail.png
layout: download
menu:
  main:
    weight: 2
name: Kontact
sources:
- description: 'Kontact je razpršen po številnih skladiščih. Oglejte si našo stran
    [Sodelujte](/get-involved) za podrobna navodila, kako pridobiti vsa skladišča
    Kontact.

    '
  name: Git
  src_icon: /reusable-assets/git.svg
- description: 'We are currently working on bringing Kontact to Windows. Do you want
    to help us? Check [how to get involved](/get-involved) and get in touch with us!

    '
  name: Windows
  src_icon: /reusable-assets/windows.svg
title: Prenos
---
