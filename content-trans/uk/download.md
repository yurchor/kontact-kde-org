---
appstream: org.kde.kontact
components: true
flatpak: true
flatpak_exp: https://community.kde.org/KDE_PIM/Flatpak
gear: true
konqi: /assets/img/konqi-mail.png
layout: download
menu:
  main:
    weight: 2
name: Kontact
sources:
- description: 'Код Kontact зберігається у декількох сховищах коду. Ознайомтеся із
    нашою [сторінкою початкових кроків участі](/get-involved), щоб дізнатися більше
    про те, як отримати код з усіх сховищ коду Kontact.

    '
  name: Git
  src_icon: /reusable-assets/git.svg
- description: 'Зараз ми працюємо над реалізацією Kontact у Windows. Хочете допомогти
    нам? Ознайомтеся із [настановами щодо участі у проєкті](/get-involved) і зв''язуйтеся
    з нами!

    '
  name: Windows
  src_icon: /reusable-assets/windows.svg
title: Отримати
---
