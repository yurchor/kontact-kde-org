---
description: ''
layout: component
shortDescription: ''
title: KNotes
weight: 50
---
KNotes je program, ktorý vám umožní písať na počítači ekvivalent "sticky notes". Poznámky sa ukladajú automaticky keď ukončíte program a potom sa zobrazia pri otvorení programu.

## Funkcie

* Písanie poznámok podľa vášho výberu písma a farby pozadia
* Použitie drag and drop na poslanie vašich poznámok
* Je možné potiahnutie do kalendára na rezervovanie termínu
* Poznámky je možné tlačiť
