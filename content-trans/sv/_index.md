---
menu:
  main:
    name: Kontact
    params:
      components:
      - background: /assets/img/kontact-menu-features.png
        large: true
        name: Funktioner i Kontact
        url: /components/kontact
      - background: /assets/img/icon-kmail.svg
        name: Kmail
        url: /components/kmail
      - background: /assets/img/icon-korganizer.svg
        name: Korganizer
        url: /components/korganizer
      - background: /assets/img/icon-kaddressbook.svg
        name: Adressbok
        url: /components/kaddressbook
      - background: /assets/img/icon-akregator.svg
        name: Akregator
        url: /components/akregator
      - background: /assets/img/icon-knotes.svg
        name: Anteckningslappar
        url: /components/knotes
      - background: /assets/img/icon-akonadi.png
        name: Akonadi
        url: /components/akonadi
    weight: 1
title: 'Kontact-sviten: Den kraftfulla lösningen för personlig informationshantering'
---
