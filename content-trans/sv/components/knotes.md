---
description: ''
layout: component
shortDescription: ''
title: Anteckningslappar
weight: 50
---
Anteckningslappar är ett program som låter dig skriva datorns motsvarighet till klisterlappar. Anteckningarna sparas automatiskt när programmet avslutas, och visas när programmet startas.

## Funktioner

* Skriv anteckningar med eget val av teckensnitt och bakgrundsfärg
* Använd drag och släpp för att skicka anteckningar med e-post
* Kan dras till en kalender för att boka en tidslucka
* Anteckningar kan skrivas ut
