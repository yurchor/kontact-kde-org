---
description: 'Kontact förenar alla våra program för personlig informationshantering
  i ett enda fönster för att ge dig bästa möjliga upplevelse och öka din produktivitet.
  Du kan använda alla verktyg i vår kraftfulla svit på ett ställe med Kontact. I motsats
  till tillverkarspecifika webbprogram värdesätter vi din integritet och stöder öppna
  standarder, vilket är orsaken till att Kontact ger dig fullständig kontroll av din
  information. Du kan förstås också komma åt informationen nedkopplad om du vill.

  '
layout: component
screenshot: /assets/img/kontact-summary.png
shortDescription: 'Hanterar all e-post, kalendrar och kontakter inne i ett enda fönster.

  '
title: Kontact
weight: 10
---
Kontact är en funktionsrik svit för personlig informationshantering skapad av [KDE](https://kde.org). Kontact förenar alla våra program för personlig informationshantering i ett enda fönster, så att du kan hantera all e-post, alla kalendrar, nyheter och annan information ännu effektivare. Du kan också använda varje komponent i Kontact som ett fristående program.

## Funktionsrik

Vi tror på att programvara ska anpassa sig till ditt arbetsflöde istället för att tvinga dig att anpassa dig till deras. Kontact är mycket inställningsbart och anpassningsbart så att du kan ställa in det exakt  på det sätt som fungerar för dig. Ta en titt på de individuella sidorna i Kontacts komponenter för att få en översikt över en del funktioner som var och en av dem kan erbjuda.

## Flexibel

Kontact stöder ett stort antal e-post-, kalender- och adressbokstjänster och protokoll, och kan enkelt utökas för att stödja ytterligare tjänster. Du behöver inte ändra e-postleverantör eller överföra din kalender någon annanstans för att synkronisera den med Kontact. Peka bara Kontact på tjänsten där din personliga information finns så visar Kontact den och håller den synkroniserad.

Kontact kan också importera personlig informationshanteringsdata från andra sviter för personlig informationshantering såsom Thunderbird och Evolution.

## Säker

Kontact stöder alla moderna säkerhetsstandarder för att skydda din integritet. All kommunikation med fjärrservrar är normalt krypterad genom att använda kryptering med industristandarden SSL/TLS. Kontact har också inbyggt stöd för signering och kryptering av e-post med GPG och S/MIME, och kan automatiskt verifiera e-postsignaturer.

Kontact är normalt inställt att skydda din integritet genom att blockera inläsning av externt innehåll, automatiska svar och andra trick som används av bluffmakare.

## Integrerad

Alla komponenter i Kontact är välintegrerade. Korganizer kan skicka mötesinbjudningar via e-postkonton inställda i Kmail. Kmail återger mötesinbjudningar och visar konflikter med händelser i dina kalendrar. Du kan blanda uppgifter och vanliga händelser i Korganizers agendavy.

## Fri och öppen källkod

Kontact är fritt (både som i yttrandefrihet och i gratis öl) och med fullständigt öppen källkod. Det betyder att [källkoden](https://invent.kde.org/pim/kontact) är tillgänglig så att alla kan se den, granska den och förbättra den.

Vi är stolta över vår öppenhet och vårt fokus på att skydda användarnas integritet. Kontact spionerar inte på dig, visar ingen reklam, lagrar inte din känsliga information någonstans i ett tillverkarspecifikt moln eller delar den med någon annan. Se [KDE:s integritetspolicy för programvara](https://kde.org/privacypolicy-apps/) för mer information.
