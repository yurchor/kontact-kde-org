---
description: 'Korganizer är en kraftfull och funktionsrik organisatör, händelse- och
  uppgiftshanterare, som är väl integrerad med resten av Kontact, i synnerhet med
  Kmail.

  '
layout: component
screenshot: /assets/img/kontact-korganizer.png
shortDescription: 'Organisera din tid! Hantera händelser, skapa uppgifter, schemalägg
  möten med mera.

  '
title: Korganizer
weight: 30
---
Korganizer är kalender- och schemaläggningskomponenten i Kontact. Den tillhandahåller hantering av händelser och uppgifter, alarm underrättelser, webbexport, nätverkstransparent hantering av data, gruppschemaläggning, import och export av kalenderfiler, med mera. Den kan arbeta ihop med ett stort antal kalendertjänster, inklusive NextCloud, Kolab, Google kalender och andra. Korganizer är helt anpassningsbar enligt dina behov och är en integrerad del av Kontact-sviten, som har målet att vara en fullständig lösning för att organisera personlig information. Korganizer stöder de två dominerande standarderna för att lagra och utbyta kalenderdata, vCalendar och iCalendar.

## Funktioner

* **Stöd för flera kalendrar och uppgiftslistor**: Korganizer kan transparent sammanfoga kalenderinformation från olika filer eller andra kalenderdatakällor, exempelvis kalendrar på webben. De kan enkelt aktiveras, inaktiveras, läggas till och tas bort från det grafiska användargränssnittet.
* **Integrering med Kontact**: Korganizer är helt integrerat med Kontact, det fullständiga programmet för personlig informationshantering. Inne i Kontact är några ytterligare funktioner tillgängliga som konvertering av e-post, händelser eller uppgifter genom att dra och släppa dem.
* **Lagringsmodell**: Korganizer har en beständig kalender. Användaren behöver inte ta hand om att läsa in eller spara kalendern. Ändringar sparas omedelbart på disk. Om kalendern ändras externt läses den automatiskt in och uppdateras i vyn. En låsningsmekanism hanterar samtidig åtkomst av kalendern.
* **Ångra och gör om.** Korganizer stöder obegränsat ångra och gör om.
* **Integrering av uppgifter i agendavyn**: Uppgifter visas i vecko- och dagsvyerna. Uppgifer kan konverteras till händelser genom att dra från uppgiftslistan och släppa på agendavyn.
* **Bilagor för händelser och uppgifter**: Referenser till webbsidor, lokala filer eller brev kan bifogas i händelser och uppgifter. Bifogad information kan enkelt kommas åt genom ett enkelklick i händelse- eller uppgiftsvyn, samt i översiktsvyn eller editorerna.
* **Snabbinmatning av uppgifter**: Ett särskilt inmatningsfält tillåter att man snabbt skapar en uppgift utan att behöva öppna en editor. Det är särskilt praktiskt för att skapa flera uppgifter i rad.
* **Snabbinmatning av händelser**: Det finns flera sätt att skapa händelser i agendavyn. Inskrivna händelser kan skapas genom att markera ett tidsintervall och sedan helt enkelt börja skriva. En editor visas och den inskrivna texten hamnar i rubriken. Alternativt kan händelseeditorn visas när markeringen av tidsintervallet är klar, och förutom de vanliga meny- och verktygsradsalternativen finns det tangentbindningar och en sammanhangsberoende meny för att visa editordialogrutan.
* **Utskriftsstöd**: Kalendrar kan skrivas ut med olika stilar. Utskrift stöder också färger och överlappande händelser.
* **Integrering med Kmail**: Kmail stöder direkt överföring av inbjudningar och andra kalenderbilagor till Korganizer.
