---
description: 'Adressboken låter dig enkelt hantera dina kontakter. Den är intimt integrerad
  med resten av Kontact, vilket låter dig enkelt välja ut kontakter när du skickar
  e-post eller skapar en mötesinbjudan.

  '
layout: component
screenshot: /assets/img/kontact-kaddressbook.png
shortDescription: Hantera enkelt dina kontakter.
title: Adressbok
weight: 30
---
Adressboken lagrar all personlig information om din familj, dina vänner och andra kontakter. Den stöder ett stort antal tjänster, inklusive NextCloud, Kolab, Google kontakter, Microsoft Exchange (EWS) eller vilken CalDAV standardserver som helst.

## Funktioner

* **Kraftfull sökning**: Adressboken har inställningsbara filter och kraftfulla sökmöjligheter.
* **Detektering av dubbletter och sammanfogning**: Adressboken kan hitta duplicerade kontakter från flera källor och kan sammanfoga dem till en enda kontakt.
* **Integrering**: Integrerar med andra komponenter i Kontact, t.ex. med export av påminnelser om födelsedagar till [KOrganizer](../korganizer).
* **QR-koder**: Adressboken kan visa en QR-kod för varje kontakt för att snabbt läsa in den i din telefon eller skicka den till någon.
* **Bra stöd för standarder**: Adressboken kan importera och exportera till nästan alla adressboksstandarder.
* **Integrering med LDAP**: Kan ansluta till flera LDAP-servrar, som sedan kan användas för automatisk komplettering av kontakter när brev skrivs i [KMail](../kmail).
