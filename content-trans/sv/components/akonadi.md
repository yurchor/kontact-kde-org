---
description: ''
layout: component
screenshot: /assets/img/akonadi.png
shortDescription: ''
title: Akonadi
weight: 70
---
Akonadi-ramverket, uppkallat efter rättvisans orakelgudinna i Ghana, ansvarar för att ge program en central databas för att lagra, indexera, och hämta användarens personliga information. Det omfattar användarens e-post, kontakter, kalendrar, händelser, journaler, alarm, anteckningar, etc.

## Funktioner

* Gemensam datacache för personlig informationshantering
* Typoberoende design
* Utökningsbar
* Generell nedkopplad åtkomst, ändra inspelning och spela upp igen
* Generell konfliktdetektering och upplösning
* Resurser går att gruppera enligt profil
* Objekt består av flera oberoende hämtbara delar
* Hämtning utan kopiering möjlig
* Samtidig åtkomst tillåter bakgrundsaktivitet oberoende av användargränssnittsklient
* Synkroniserar e-post, kalender, adressbok med fjärrservrar
* Tillåter semantisk skrivbordsinfrastruktur att komma åt data från personlig informationshantering
* Arkivering
* Indexering
* Sökning utanför process
* Design med flera processer
* Kraschisolering
* Stora objekt kan inte blockera hela systemet
* Länkning med IPC tillåter tillverkarspecifika komponenter
