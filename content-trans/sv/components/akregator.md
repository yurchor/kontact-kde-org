---
description: 'Akregator är en RSS-kanalläsare. Den kan automatiskt ladda ner RSS-kanaler
  från dina favoritplatser och låter dig läsa artiklar även när du är nedkopplad.
  Den tillhandahåller också omfattande sökfunktionalitet och arkiveringsfunktioner.

  '
layout: component
screenshot: /assets/img/kontact-akregator.png
shortDescription: Håll reda på nyheter med vår RSS-läsare
title: Akregator
weight: 40
---
Akregator hjälper till att hålla dig informerad om nyheter på webbplatser som KDE Dot News och Planet KDE bloggar. Teknologin som används är RSS och många platser stöder den.

## Funktioner

* Lättanvänd
* Kan hantera dussintals kanaler
* Kan underrätta dig om olästa kanaler
* Använder flikar för att ge tillgång till intern läsning av hela artiklar
* Kanalarkiv
* Importera och exportera kanaler
