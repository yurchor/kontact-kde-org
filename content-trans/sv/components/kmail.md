---
description: 'Kmail är en toppmodern e-postprogram som är väl integrerad med använda
  i stor utsträckning, såsom GMail. Det tillhandahåller många verktyg och funktioner
  för att maximera din produktivitet och gör det snabbt och enkelt att arbeta med
  stora e-postkonton. Kmail stöder ett stort antal e-postprotokoll: POP3, IMAP, Microsoft
  Exchange (EWS) med flera.

  '
layout: component
screenshot: /assets/img/kontact-kmail.png
shortDescription: Öka din produktivitet med ett funktionsrikt e-postprogram.
title: Kmail
weight: 20
---
Kmail är e-postkomponenten i Kontact, den integrerade personliga informationshanteraren från KDE.

## Säker

* **Säkra förval**: Förvalda inställningar väljer hellre bort funktioner än äventyrar säkerhet och integritet.
* **Kryptering från början till slut**: Stöd för OpenPGP och S/MIME är inbyggt, och en guide för inställning och nyckelgenerering hjälper användare komma igång med det.
* **Överföringskryptering**: SSL/TLS-krypterad kommunikation stöds naturligtvis, liksom ett antal metoder för behörighetskontroll såsom GSSAPI (Kerberos) och OAuth2.
* **Stark isolering av HTML-innehåll**: Om HTML-brev inte kan undvikas, säkerställer Kmail att inga externa referenser kan döljas där för att läcka information eller äventyra din integritet. Dessutom varnar Kmails skydd mot nätfiske om suspekta länkar i sådana brev.
* **Skydd mot skräppost**: Om din e-postserver inte redan tar hand om det, kan Kmail lokalt integrera populära verktyg för kontroll av skräppost såsom SpamAssassin eller Bogofilter.

## Kraftfull

* **Hanterar nedkoppling**: Kmail låter dig fungera helt utan nätverksanslutning genom att (valfritt) synkronisera all e-post lokalt.
* **Flera identiteter**: Kmail separerar identiteter från konton, vilket ger dig stor flexibilitet om du har olika roller i flera organisationer.
* **Mallar**: Ett flexibelt system med mallar och textsnuttar stöder dig med att delvis automatisera brevskrivning.
* **Flerspråklig**: Inbyggt stöd för översättning av innehåll och stöd för stavningskontroll med automatisk språkdetektering per stycke hjälper dig när du hanterar innehåll på flera språk.
* **Produktivt brevfönster**: Kmails brevfönster levereras med många små hjälpfunktioner för att göra dig effektivare, såsom varningar om bortglömda bilagor, komprimering av bilagor på plats eller storleksändring av bifogade bilder.
* **Kraftfull filtrering**: Förutom att automatisera sortering av e-post enligt en mängd olika kriterier, kan Kmails filtreringssystem också användas för att implementera godtyckliga arbetsflöden, eftersom det kan integreras med externa program.
* **Sökning och etikettering**: Att flagga och etikettera brev hjälper till med sortering och informationshämtning, och det gör även de kraftfulla lokala och fjärrsökmöjligheterna.
* **Hantering av sändlistor**: Mailman sändlistor kan detekteras automatiskt och vanliga åtgärder för att hantera sändlistor är direkt tillgängliga i Kmail.
* **Flexibel inställning**: Otaliga inställningsalternativ låter dig finjustera många aspekter av Kmail för att exakt passa dina behov och önskemål.

## Integrerad

* **Inbjudningar med iTip**: Brev med mötesinbjudningar detekteras och erbjuder snabba svar på plats, och matas naturligtvis in i [Korganizer](../korganizer).
* **Adressbok**: Förutom att automatiskt komplettera adresser från [addressboken](../kaddressbook), använder Kmail också avatarer och kryptoinställningar därifrån.
* **Bokningsbrev**: Kmail kan detektera flyg eller hotellreservationer i brev från vissa leverantörer och erbjuder att lägga till dem i din kalender.

## Följer standarder

* Stöder standardprotokollen för e-post IMAP, POP3 och SMTP.
* Stöder skicka e-post (IMAP IDLE).
* Fullständigt stöd för sökning i IMAP-korgar.
* Inbyggt stöd för OpenPGP, PGP/MIME och S/MIME på plats.
* Stöd för Sieve filtrering på serversidan.
