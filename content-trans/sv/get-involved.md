---
getintouch: Utvecklarna av Kontact håller i allmänhet till på IRC-kanalerna [#kontact](irc://irc.libera.chat/kontact)
  och [#akonadi](irc://irc.libera.chat/akonadi) på Libera Chat. De flesta utvecklingsrelaterade
  diskussionerna äger rum på [sändlistan kde-pim](https://mail.kde.org/mailman/listinfo/kde-pim/).
  Gå helt enkelt med, säg hej och tala om för oss vad du vill hjälpa till med!
layout: get-involved
menu:
  main:
    weight: 4
name: Kontact
title: Engagera dig
userbase: Kontact
---
Kontacts källkod är spridd över många komponenter i många arkiv: det är lätt att bli vilsen och överväldigad om du är ny. Om du vill engagera dig i utvecklingen av Kontact, läs materialet som länkas från den här sidan och ta kontakt med oss: vi hjälper dig att hitta i vår kodbas.

**Börja gärna med att läsa en del av informationen på [KDE-gemenskapens wiki för personlig informationshantering](https://community.kde.org/KDE_PIM).**

## Uppgifter för nykomlingar

Söker du efter några enkla uppgifter för att komma igång med att förbättra Kontact? Ta då en titt på våra [juniorjobb](https://phabricator.kde.org/tag/kde_pim_junior_jobs/). Vi har samlat ett antal uppgifter som är ganska lätta och isolerade, och som inte kräver en djupare förståelse av hela kodbasen och arkitekturen: perfekta ingångspunkter i Kontacts underbara värld.

Om du skulle vilja börja arbeta med någon uppgift, tilldela den gärna till dig själv (om den inte redan är tilldelad till någon annan) och tala om det för oss i kommentarerna under uppgiften.
