---
appstream: org.kde.kontact
components: true
flatpak: true
flatpak_exp: https://community.kde.org/KDE_PIM/Flatpak
gear: true
konqi: /assets/img/konqi-mail.png
layout: download
menu:
  main:
    weight: 2
name: Kontact
sources:
- description: 'Kontact är spritt över en mängd olika arkiv. Ta en titt på vår sida
    [Engagera dig](/get-involved) för detaljerade instruktioner om hur man hämtar
    alla Kontact-arkiv.

    '
  name: Git
  src_icon: /reusable-assets/git.svg
- description: 'Vi arbetar för närvarande på att föra Kontact till Windows. Vill du
    hjälpa oss? Ta reda på [hur du kan engagera dig](/get-involved) och ta kontakt
    med oss!

    '
  name: Windows
  src_icon: /reusable-assets/windows.svg
title: Nerladdning
---
