---
appstream: org.kde.kontact
components: true
flatpak: true
flatpak_exp: https://community.kde.org/KDE_PIM/Flatpak
gear: true
konqi: /assets/img/konqi-mail.png
layout: download
menu:
  main:
    weight: 2
name: Kontact
sources:
- description: 'Kontact is spread across a multitude of repositories. Check our [Getting
    Involved](/get-involved) page for detailed instructions how to get all Kontact
    repositories.

    '
  name: Git
  src_icon: /reusable-assets/git.svg
- description: 'We are currently working on bringing Kontact to Windows. Do you want
    to help us? Check [how to get involved](/get-involved) and get in touch with us!

    '
  name: Windows
  src_icon: /reusable-assets/windows.svg
title: Descargar
---
