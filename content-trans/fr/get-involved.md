---
getintouch: 'Les développeurs de Kontact sont généralement présents sur les forums
  de discussion suivants : [#kontact](irc://irc.libera.chat/kontact) et [#akonadi](irc://irc.libera.chat/akonadi)
  sur le canal « IRC » de « Libera Chat ». La plupart des discussions liées au développement
  ont lieu sur la [liste de diffusion « kde-pim »](https://mail.kde.org/mailman/listinfo/kde-pim/).
  Rejoignez-nous, dites bonjour et dites-nous ce que vous aimeriez faire pour nous
  aider ! '
layout: get-involved
menu:
  main:
    weight: 4
name: Kontact
title: Impliquez-vous
userbase: Kontact
---
Le code source de Kontact est réparti sur de nombreux composants dans de nombreux dépôts. Il est facile de s'y perdre et d'être submergé si vous êtes novice en la matière. Si vous voulez participer au développement de Kontact, veuillez simplement lire les documents concernant cette page et contactez-nous - nous vous aiderons à trouver dans votre parcours dans notre base de code.

**Veuillez commencer par lire quelques informations sur le [wiki de la communauté « PIM » de KDE](https://community.kde.org/KDE_PIM)**.

## Tâches pour novices

Vous cherchez des tâches simples pour commencer à améliorer Kontact ? Alors, veuillez consulter nos offre [d'emplois pour débutant(e)s](https://phabricator.kde.org/tag/kde_pim_junior_jobs/). Nous avons rassemblé un certain nombre de tâches assez simples et isolées, ne nécessitant pas une compréhension approfondie de l'ensemble de la base de code et de l'architecture. Un point d'entrée idéal dans le monde merveilleux de Kontact.

Si vous souhaitez commencer à travailler sur une tâche, n'hésitez pas à vous l'attribuer (à moins qu'elle ne soit déjà attribuée à quelqu'un d'autre) et faites-le nous savoir dans les commentaires sous la tâche.
