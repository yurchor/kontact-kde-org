---
description: 'Kontact unifie toutes nos applications « PIM » en une seule fenêtre
  pour vous offrir la meilleure expérience possible et augmenter votre productivité.
  Avec Kontact, vous pouvez utiliser tous les outils de notre puissante suite en un
  seul endroit. Contrairement aux applications Internet propriétaires, nous respectons
  votre vie privée et prenons en charger les standards ouverts. C''est la raison pour
  laquelle Kontact vous donne un contrôle total sur vos données. Vous pouvez bien
  sûr accéder à vos données hors ligne si vous le souhaitez.

  '
layout: component
screenshot: /assets/img/kontact-summary.png
shortDescription: 'Gérez tous vos courriels, agendas et contacts dans une seule fenêtre.

  '
title: Kontact
weight: 10
---
Kontact est une suite pour la gestion des informations personnelles, riche en fonctionnalités et créée par [KDE](https://kde.org). Kontact unifie toutes nos applications « PIM » dans une unique fenêtre, afin que vous puissiez traiter vos courriels, votre agenda, vos nouvelles et d'autres données de manière encore plus efficace. Vous pouvez également exécuter chacun des composants de Kontact comme une application autonome.

## Riche en fonctionnalités

Nous pensons que les logiciels doivent s'adapter à vos flux de travail, au lieu de vous forcer à vous adapter aux leurs. Kontact est hautement configurable et personnalisable de sorte que vous puissiez l'installer exactement de la manière qui vous convient le mieux. Veuillez consulter les pages individuelles des composants de Kontact pour avoir une vue d'ensemble de certaines des fonctionnalités que chacun d'eux peut offrir.

## Flexible

Kontact prend en charge une grande variété de services et de protocoles de courrier électronique, d'agenda et de carnet d'adresses. Il peut être facilement étendu pour prendre en charge des services supplémentaires. Vous n'avez pas besoin de changer de fournisseur de messagerie ou de migrer votre agenda ailleurs pour le synchroniser avec Kontact. Il suffit de faire pointer Kontact vers le service où se trouvent vos données « PIM » et Kontact les affichera et les maintiendra synchronisées.

Kontact peut également importer vos données « PIM » et certains paramètres à partir d'autres suites « PIM » comme Thunderbird et Evolution.

## Sécurisé 

Kontact prend en charge toutes les normes de sécurité modernes afin de protéger votre vie privée. Toutes les communications avec les serveurs distants sont chiffrées par défaut en utilisant le standard « SSL / TLS ». Kontact dispose également d'une prise en charge intégrée pour la signature et le chiffrement des courriels avec « GPG » et « S / MIME » et il peut vérifier automatiquement les signatures des courriels.

Kontact est configuré par défaut pour protéger votre vie privée en bloquant le chargement de contenu externe, des réponses automatiques et d'autres astuces souvent utilisées par les escrocs.

## Intégré

Tous les composants de Kontact sont parfaitement intégrés ensemble. KOrganizer peut envoyer des invitations à des réunions grâce à des comptes de messagerie configurés dans KMail. KMail diffusera les invitations à des réunions et vous montrera les conflits avec les évènements de vos agendas. Vous pouvez mélanger vos tâches à faire et vos évènements récurrents dans la vue « Agenda » de KOrganizer.

## Libre et Open Source

Kontact est libre (à la fois dans le sens de liberté d'expression et de bière gratuite) et complètement Open source. Cela signifie que le [code source](https://invent.kde.org/pim/kontact) est disponible pour que tout le monde puisse le voir, le vérifier et l'améliorer.

Nous sommes fiers de notre ouverture et de notre souci de protéger la vie privée des utilisateurs. Kontact ne vous espionnera pas, n'affichera aucune publicité, n'enregistrera aucune de vos données sensibles, quelque part dans un nuage propriétaire ou les partager avec d'autres personnes. Veuillez consulter la [Politique de confidentialité des logiciels de KDE](https://kde.org/privacypolicy-apps/) pour plus de détails.
