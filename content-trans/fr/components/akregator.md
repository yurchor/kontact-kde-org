---
description: 'Akregator est un lecteur de flux « RSS ». Il peut télécharger automatiquement
  les flux « RSS » de vos sites préférés. Il vous permet de lire les articles même
  lorsque vous êtes hors ligne. Il offre également des fonctionnalités très riches
  de recherche et d''archivage.

  '
layout: component
screenshot: /assets/img/kontact-akregator.png
shortDescription: Restez au courant de l'actualité grâce à notre lecteur de flux « RSS ».
title: Akregator
weight: 40
---
Akregator vous aide à rester informé des nouvelles histoires sur les sites Internet comme KDE, Dot News et les blogs de Planet KDE. La technologie utilisée est le « RSS » et de nombreux sites la prennent en charge.

## Fonctionnalités

* Facile à utiliser
* Peut accepter des dizaines de flux
* Peut vous informer des flux non lus.
* Utilise des onglets pour donner accès à la lecture interne des discussions complètes
* Archive des flux
* Importer et exporter des flux
