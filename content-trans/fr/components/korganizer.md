---
description: 'KOrganizer est un organisateur, un gestionnaire d''évènements et de
  tâches à faire, puissant et riche en fonctionnalités, s''intégrant parfaitement
  avec le reste de Kontact, en particulier avec KMail.

  '
layout: component
screenshot: /assets/img/kontact-korganizer.png
shortDescription: 'Organisez votre temps ! Gérez vos évènements et vos tâches à faire,
  planifiez vos réunions et bien plus encore.

  '
title: KOrganizer
weight: 30
---
KOrganizer est le composant pour agendas et planification de Kontact. Il réalise la gestion des évènements et des tâches, la notification des alarmes, l'exportation vers Internet, la gestion transparente des données via le réseau, la planification de groupes, l'importation et l'exportation de de gestion d'agendas, dont « NextCloud », « Kolab », « Google Calendar » et beaucoup d'autres. KOrganizer est entièrement personnalisable selon vos besoins et fait partie intégrante de la suite Kontact, dont le but est d'être une solution complète pour l'organisation de vos vos données personnelles. KOrganizer prend en charge les deux standards dominants pour l'enregistrement et l'échange de données d'agendas, « vCalendar » et « iCalendar ».

## Fonctionnalités

* **Prise en charge de plusieurs agendas et listes de tâches**. Korganizer peut fusionner de manière transparente, les données d'agendas provenant de différents fichiers ou d'autres sources de données d'agendas, comme par exemple des agendas sur Internet. Ils peuvent être commodément activés, désactivés, ajoutés et supprimés à partir de l'interface graphique utilisateur.
* **Intégration de Kontact**. KOrganizer est totalement intégré à Kontact, l'application complète de gestion des informations personnelles. Dans Kontact, certaines fonctions supplémentaires sont disponibles, comme la conversion de courriels en évènements ou en tâches à faire par glisser-déposer.
* **Modèle d'enregistrement**. KOrganizer possède un agenda persistant. L'utilisateur n'a pas à s'occuper du chargement ou de l'enregistrement de l'agenda. Les modifications sont immédiatement enregistrées sur le disque. Si l'agenda est modifié de manière externe, il est automatiquement chargé et mis à jour dans la vue. Un mécanisme de verrouillage gère l'accès concurrent à l'agenda.
* **Annuler et refaire**. KOrganizer prend en charge un nombre illimité d'annulations et de réitérations.
* **Intégration des tâches dans l'affichage d'agendas**. Les tâches à faire sont affichées dans l'affichage en semaines et en jours. Les tâches à faire peuvent être converties en évènements en les faisant glisser depuis la liste des tâches à faire et en les déposant sur l'affichage de l'agenda.
* **Pièces jointes pour des évènements ou des tâches à faire**. Les références à des pages Internet, des fichiers locaux ou des courriers électroniques peuvent être jointes à des évènements et à des tâches. Les données jointes peuvent facilement être facilement accessibles par un simple clic à partir des vues d'évènements et de tâches à faire, y compris avec la vue de résumé ou avec les éditeurs.
* **Entrée rapide pour tâches à faire**. Un champ spécial d'entrée permet de créer rapidement une tâche à faire sans avoir à ouvrir un éditeur. C'est particulièrement pratique pour créer plusieurs tâches à faire à la suite.
* **Entrée rapide d'évènement**. Il existe plusieurs manières de créer des évènements à partir de la vue de l'agenda : des évènements par saisie anticipée peuvent être créés en sélectionnant une plage horaire et en commençant simplement à saisie. Un éditeur s'ouvrira et le texte saisi sera placé dans le titre. De façon optionnelle, l'éditeur d'évènements peut être ouvert lorsque la sélection de temps est terminée et qu'en plus des entrées habituelles du menu et de la barre d'outils, il y a des associations de touches et un menu contextuel pour démarrer le boîte de dialogue de l'éditeur.
* **Prise en charge de l'impression**. Les agendas peuvent être imprimés en utilisant différents styles. L'impression prend également en charge les couleurs et les évènements se chevauchant.
* **Intégration de KMail**. KMail prend directement en charge le transfert des invitations et autres pièces jointes de l'agenda vers KOrganizer.
