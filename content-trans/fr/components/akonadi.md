---
description: ''
layout: component
screenshot: /assets/img/akonadi.png
shortDescription: ''
title: Akonadi
weight: 70
---
L'environnement de développement Akonadi, nommé d'après la déesse oracle de la justice au Ghana est chargé de fournir aux applications, une base de données centralisée pour l'enregistrement et le chargement des informations personnelles de l'utilisateur. Cela comprend les éléments suivants : courriels, contacts, agendas, évènements, journaux, alarmes, notes, etc.

## Fonctionnalités

* Cache commun de données « PIM »
* Conception de type agnostique
* Extensibilité
* Accès générique hors ligne, enregistrement et relecture des modifications
* Détection et résolution générique des conflits
* Les ressources peuvent être regroupées par profil
* Articles composés de parties multiples pouvant être chargées indépendamment
* Possibilité de récupérer des zéro-copies
* L'accès concurrent rend une activité en arrière-plan indépendante du client pour l'interface utilisateur.
* Synchronisation du courrier électronique, de l'agenda et du carnet d'adresses avec des serveurs distants
* Permet à l'infrastructure de bureau sémantique d'accéder aux données « PIM »
* Archivage
* Indexation
* Recherche hors-processus
* Conception multi-processus
* Isolation des plantages
* Les articles volumineux ne peuvent pas bloquer tout le système
* La liaison par « IPC » permet des composants propriétaires
