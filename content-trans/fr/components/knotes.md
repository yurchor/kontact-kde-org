---
description: ''
layout: component
shortDescription: ''
title: KNotes
weight: 50
---
KNotes est un programme vous permettant d'écrire l'équivalent informatique de notes autocollantes. Les notes sont enregistrées automatiquement lorsque vous quittez le programme. Elles s'affichent lorsque vous ouvrez le programme.

## Fonctionnalités

* Rédigez des notes dans la police et la couleur de fond de votre choix.
* Utilisez la fonction de glisser-déposer pour envoyer vos notes par courriel
* Peut être glissé dans le calendrier pour réserver un créneau horaire.
* Les notes peuvent être imprimées.
