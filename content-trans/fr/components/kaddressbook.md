---
description: 'KAddressBook vous permet de gérer facilement vos contacts. Il est profondément
  profondément intégré au reste de Kontact, ce qui vous permet de sélectionner facilement
  vos contacts lors de l''envoi d''un courriel ou de la création d''une invitation
  à une réunion.

  '
layout: component
screenshot: /assets/img/kontact-kaddressbook.png
shortDescription: Gérer facilement vos contacts.
title: KAddressBook
weight: 30
---
KAddressBook enregistre toutes les informations personnelles de votre famille, de vos amis et de vos autres contacts. Il prend en charge une grande variété de services, notamment « NextCloud », « Kolab », « Google Contacts », « Microsoft Exchange (EWS) ou tout serveur « CalDAV » standard.

## Fonctionnalités

* **Recherche avancée - KAddressBook possède des filtres personnalisables et des fonctions de recherche avancées.
* **Détection et fusion des contacts dupliqués**. KAddressBook peut identifier les contacts dupliqués entre de multiples sources et les fusionner comme un seul contact.
* **Intégration**. Prend en charge l'intégration avec d'autres composants de Kontact, par exemple, l'exportation des rappels d'anniversaire vers [KOrganizer](../korganizer).
* **Codes QR**. KAddressbook peut afficher un QR code pour chaque contact afin de le scanner rapidement dans votre téléphone ou de l'envoyer à quelqu'un.
* **Bonne prise en charge des standards**. KAddresbook peut importer et exporter des contacts depuis et vers presque toutes les standards de carnets d'adresses.
* **Intégration « LDAP »**. Peut se connecter à plusieurs serveurs « LDAP », pouvant alors être utilisés pour l'auto-complètement des contacts lors de la composition de courriels dans [KMail](../kmail).
