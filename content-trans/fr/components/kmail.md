---
description: 'KMail est un client de messagerie ultramoderne s''intégrant parfaitement
  aux fournisseurs largement utilisés de messagerie électronique comme GMail. Il fournit
  de nombreux outils et fonctionnalités pour maximiser votre productivité et pour
  travailler avec de grands comptes de messagerie, de façon simple et rapide. KMail
  prend en charge une grande variété de protocoles de messagerie, comme « POP3 »,
  « IMAP », « Microsoft Exchange (EWS) » et de nombreux autres.

  '
layout: component
screenshot: /assets/img/kontact-kmail.png
shortDescription: Augmentez votre productivité avec une application de messagerie
  riche en fonctionnalités.
title: KMail
weight: 20
---
KMail est le composant de messagerie de Kontact, le gestionnaire d'informations personnelles, intégré de KDE.

## Sécurisé 

* **Sécurité par défaut**. Les paramètres par défaut font plus de compromis sur les fonctionnalités que sur la sécurité ou la confidentialité.
* **Chiffrement de bout en bout**. La prise en charge de « OpenPGP » et de » S / MIME » est intégré et un assistant de configuration et de génération de clés aide les utilisateurs à démarrer.
* **Chiffrement du transport**. Les communications chiffrées avec « SSL / TLS » sont bien sûr prises en charge, tout comme un certain nombre de méthodes d'authentification telles que « GSSAPI » (Kerberos) ou « OAuth2 ».
* **Forte séparation du contenu « HTML »**. Si les courriels avec des éléments « HTML » ne peuvent être évités, KMail garantit qu'aucune référence externe ne peut y être cachée et entraîner une fuite d'informations ou de compromission de votre vie privée. De plus, les protections contre l'hameçonnage de KMail vous préviennent en cas de liens suspects dans ces courriels.
* **Protection contre les pourriels**. Si votre serveur de messagerie ne s'en occupe pas déjà, KMail peut intégrer en local, des contrôleurs de pourriels populaires tels que SpamAssassin ou Bogofilter.

## Puissant

* **Capacité hors-ligne**. KMail vous permet de fonctionner entièrement sans connexion réseau en synchronisant (en option), en local, l'ensemble de votre courrier électronique.
* **Identités multiples**. KMail sépare les identités des comptes, ce qui vous donne une grande flexibilité lorsque vous avez différents rôles dans plusieurs organisations.
* **Modèles**. Un système flexible de modèles et d'extraits de texte vous aide à automatiser certaines parties de la rédaction de vos courriels.
* **Multilingue**. Prise en charge d'une traduction intégrée du contenu et détection automatique de la langue de vérification de l'orthographe par paragraphe, lorsque vous traiter du contenu dans plusieurs langues.
* **Compositeur productif**. Le compositeur de courriels de KMail est livré avec de nombreux petits assistants pour vous rendre plus efficace, comme l'avertissement sur les pièces jointes oubliées, la compression des pièces jointes en ligne ou le redimensionnement des images jointes.
* **Puissant filtrage**. Outre l'automatisation du tri de vos courriels en fonction d'une multitude de conditions, le système de filtrage de KMail peut également être utilisé pour mettre en œuvre des flux de travail arbitraires, puis qu'l peut s'intégrer à des applications externes.
* **Marquage et étiquetage**. Le marquage et l'étiquetage des messages facilitent le tri et la recherche d'informations, tout comme les puissantes fonctionnalités de recherche en local et à distance.
* **Gestion des listes de diffusion**. Les listes de diffusion de Mailman peuvent être détectées automatiquement et les opérations courantes de gestion des listes de diffusion sont directement disponibles dans l'application KMail.
* **Configuration flexible**. D'innombrables options de configuration vous permettent de régler de nombreux aspects de KMail, pour s'adapter parfaitement à vos besoins et à vos souhaits.

## Intégré

* **Invitations « iTip »**. Les courriels d'invitation à des réunions sont détectés et proposent des réponses rapides intégrées et sont bien sûr insérés dans [KOrganizer](../korganizer).
* **Carnet d'adresses**. En plus de l'auto-complètement des adresses à partir de [KAddressBook](../kaddressbook), KMail utilise aussi les avatars et les préférences de chiffrement qui s'y trouvent.
* **Courriels de réservation**. KMail peut détecter les réservations de vol ou d'hôtel dans les courriels de certains fournisseurs et proposer de les ajouter à votre calendrier.

## Conforme aux standards

* Prend en charge les protocoles de messagerie standards « IMAP », « POP3 » et « SMTP ».
* Prend en charge les courriels en « PUSH » (IMAP IDLE).
* La recherche dans les dossiers « IMAP » est totalement prise en charge.
* Prise en charge native et intégrée de « OpenPGP », « PGP / MIME » et « S / MIME ».
* Prise en charge du filtrage côté serveur avec « Sieve »
