---
appstream: org.kde.kontact
components: true
flatpak: true
flatpak_exp: https://community.kde.org/KDE_PIM/Flatpak
gear: true
konqi: /assets/img/konqi-mail.png
layout: download
menu:
  main:
    weight: 2
name: Kontact
sources:
- description: 'Kontact est réparti sur une multitude de dépôts. Veuillez consulter
    notre page [Impliquez vous ! ](/get-involved) pour des instructions détaillées
    sur la façon d''obtenir tous les dépôts de Kontact.

    '
  name: Git
  src_icon: /reusable-assets/git.svg
- description: "Nous travaillons actuellement au portage de Kontact sous Windows.\
    \ Voulez-vous nous aider ? Veuillez consulter la page [comment s'impliquer](/get-involved)\
    \ et contactez-nous ! \n"
  name: Windows
  src_icon: /reusable-assets/windows.svg
title: Télécharger
---
