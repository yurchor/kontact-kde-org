---
getintouch: De ontwikkelaars van Kontact hangen in het algemeen rond in de IRC-kanalen
  [#kontact](irc://irc.libera.chat/kontact) en [#akonadi](irc://irc.libera.chat/akonadi)
  op Libera Chat. De meeste discussies gerelateerd aan ontwikkeling vinden plaats
  op de [kde-pim e-maillijst](https://mail.kde.org/mailman/listinfo/kde-pim/). Doe
  mee, zeg hallo en vertel ons waarmee u ons zou willen helpen!
layout: get-involved
menu:
  main:
    weight: 4
name: Kontact
title: Doe mee
userbase: Kontact
---
De broncode van Kontact is verspreid over vele componenten in veel opslagruimten - het is gemakkelijk om verloren en beduusd te raken als u er nieuw in bent. Als u mee wil doen in de ontwikkeling van Kontact lees dan het materiaal waarnaar wordt verwezen vanuit deze pagina en neem contact met ons op - we zullen u helpen bij het vinden van uw weg in onze broncode.

**Begin met het lezen van enige informatie op de [KDE PIM Community wiki](https://community.kde.org/KDE_PIM).**

## Taken voor beginners

Zoeken naar enige eenvoudige taken om te beginnen met het verbeteren van Kontact? Kijk dan op [junior jobs](https://phabricator.kde.org/tag/kde_pim_junior_jobs/). We hebben een aantal taken verzamelt die tamelijk eenvoudig en geïsoleerd zijn en geen diep begrijpen van de gehele codebasis en architectuur vereisen - een perfecte ingang in de prachtige wereld van Kontact.

Als u zou willen beginnen aan het werken aan een taak, voel u dan vrij om het aan uzelf toe te kennen (tenzij het al is toegekend aan iemand anders) en laat het ons weten in het commentaar onder de taak.
