---
description: 'Akregator is een RSS-feed-reader. Het kan automatisch RSS-feeds downloaden
  uit uw favoriete sites en biedt u het lezen van artikelen zelfs wanneer u offline
  bent. Biedt ook een rijke zoekfunctionaliteit en archiveringsfunctie.

  '
layout: component
screenshot: /assets/img/kontact-akregator.png
shortDescription: Blijf op de hoogte van het nieuws met onze RSS-reader.
title: Akregator
weight: 40
---
Akregator helpt u geïnformeerd te blijven over nieuwe verhalen op websites zoals KDE Dot News en Planet KDE blogs. De gebruikte technologie is RSS en vele sites ondersteunen het.

## Mogelijkheden

* Eenvoudig te gebruiken
* Kan tientallen feeds accepteren
* Kan u over ongelezen feeds informeren
* Gebruikt tabs om toegang te geven tot intern lezen van volledige verhalen
* Archief van feeds
* Feeds importeren en exporteren
