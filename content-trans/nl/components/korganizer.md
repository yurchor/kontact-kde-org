---
description: 'KOrganizer is een krachtige organisator, rijk aan mogelijkheden, beheerder
  van afspraken en te-doens, die goed integreert met de rest van Kontact, speciaal
  met KMail.

  '
layout: component
screenshot: /assets/img/kontact-korganizer.png
shortDescription: 'Organiseer uw tijd! Beheer afspraken, te-doens, plan vergaderingen
  en meer.

  '
title: KOrganizer
weight: 30
---
KOrganizer is de agenda- en planningcomponent van Kontact. Het biedt beheer van afspraken en taken, melding van herinneringen, naar het web exporteren, netwerktransparant behandelen van gegevens, planning van groepen, im- en exporteren van agendabestanden en meer. Het is in staat om samen te werken met een brede variëteit van agnederingsservices, inclusief NextCloud, Kolab, Google Calendar en anderen. KOrganizer is volledig aan uw behoeften aan te passen en is een integraal onderdeel van de Kontact-suite, die zich richt op een volledige oplossing voor het organiseren van uw persoonlijke gegevens. KOrganizer ondersteunt de twee dominante standaarden voor opslaan en uitwisselen van agendagegevens, vCalendar en iCalendar.

## Mogelijkheden

* **Ondersteuning voor meerdere agenda's en te-doenlijsten.** Korganizer kan transparent agendagegevens uit verschillende bestanden of andere agendagegevensbronnen mengen, bijvoorbeeld agenda's op het web. Ze kunnen gemakkelijk geactiveerd, gedeactiveerd, toegevoegd en verwijderd worden uit de grafische gebruikersinterface.
* **Integratie in Kontact.** KOrganizer is volledig geïntegreerd met Kontact, de toepassing voor compleet beheer van persoonlijke informatie. In Kontact zijn enige extra functies beschikbaar zoals conversie van e-mails naar afspraken of te-doens door slepen en loslaten.
* **Opslagmodel.** KOrganizer heeft een blijvende agenda. De gebruiker hoeft niet te zorgen voor het laden of opslaan van de agenda. Wijzigingen worden onmiddellijk op schijf opgeslagen. Als de agenda extern is gewijzigd, wordt het automatisch geladen en bijgewerkt in de weergave. Een vergrendelmechanisme behandelt gelijktijdige toegang tot de agenda.
* **Ongedaan maken en opnieuw doen.** KOrganizer ondersteunt onbeperkt ongedaan maken en opnieuw doen.
* **Integratie van te-doens met agendaweergave.** Te-doens worden getoond in de week- en dagweergaven. Te-doens kunnen geconverteerd worden naar afspraken door uit de te-doen-lijst te slepen en los te laten op de agendaweergave.
* **Bijlagen voor afspraken en te-doens.** Verwijzingen naar webpagina's, locale bestanden of e-mails kunnen bijgevoegd worden bij afspraken en te-doens. De bijgevoegde gegevens zijn gemakkelijk toegankelijk met een enkele klik in de weergave van afspraken en te-doens evenals de overzichtsweergave of de bewerkers
* **Snelle invoer van te-doen.** Een speciaal invoerveld biedt het snel aanmaken van een te-doen zonder het openen van een bewerker. Dit is speciaal handig voor het aanmaken van meerdere te-doens achter elkaar.
* **Snelle invoer van afspraken.** Er zijn verschillende manieren om afspraken in de agenda te bekijken: Afspraak door gewoon intypen kan gemaakt worden door een tijdreeks te selecteren en dan gewoon met typen beginnen. Een bewerker zal geopend worden en de getypte tekst zal in de titel gezet worden. Optioneel kan de afspraakbewerker geopend worden wanneer de tijdselectie is beëindigd en naast het gebruikelijke invoermenu en werkbalk er bindingen aan toetsen zijn en een contextmenu om de bewerkerdialoog te beginnen.
* **Ondersteuning voor afdrukken.** Agenda's kunnen afgedrukt worden in verschillende stijlen. Afdrukken ondersteunt ook kleuren en overlappende afspraken.
* **Integratie in KMail.** KMail ondersteunt overdracht van uitnodigingen en andere agendabijlagen naar KOrganizer.
