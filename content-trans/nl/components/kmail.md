---
description: 'KMail is een zeer moderne e-mailclient die goed integreert met breed
  gebruikte e-mailleveranciers zoals GMail. Het biedt vele hulpmiddelen en functies
  om uw productiviteit te maximaliseren en maakt werken met grote e-mailaccounts gemakkelijk
  en snel. KMail ondersteunt een grote variëteit van e-mailprotocollen - POP3, IMAP,
  Microsoft Exchange (EWS) en meer.

  '
layout: component
screenshot: /assets/img/kontact-kmail.png
shortDescription: Vergroot uw productiviteit met een toepassing rijk mogelijkheden
  voor e-mail.
title: KMail
weight: 20
---
KMail is de e-mailcomponent van Kontact, de geïntegreerde persoonlijke informatiebeheerder uit KDE.

## Is beveiligd

* **Veilige standaarden.** Standaard instellingen bieden liever minder mogelijkheden dan inboeten op veiligheid of privacy.
* **Eind-tot-eind versleuteling.** Ondersteuning voor OpenPGP en S/MIME is ingebouwd en een assistent voor opzetten en generatie van sleutels helpt gebruikers in hiermee beginnen.
* **Versleuteling bij transporteren.** Met SSL/TLS versleutelde communicatie wordt natuurlijk ondersteund, evenals een aantal authenticatiemethoden zoals GSSAPI (Kerberos) of OAuth2.
* **Sterke isolatie van HTML inhoud.** Als e-mails met HTML niet vermeden kan worden, verzekert KMail dat er geen externe verwijzingen verborgen in verborgen kunnen worden om informatie te lekken of uw privacy compromitteren. Bovendien zal de beveiliging tegen phising van KMail waarschuwen over verdachte koppelingen in zulke e-mails.
* **Spambescherming.** Als uw e-mailserver hier niet als tegen beschermt, kan KMail populaire spamfilters zoals SpamAssassin of Bogofilter lokaal integreren.

## Krachtig

* **Kan offline werken.** KMail biedt u om geheel zonder netwerkverbinding te werken door (optioneel) uw gehele e-mail lokaal te synchroniseren.
* **Meerdere identiteiten.** KMail scheidt identiteiten van accounts, en geeft u daarmee heel wat flexibiliteit bij het dragen van verschillende petten in meerdere organisaties.
* **Sjablonen.** Een systeem met flexibele sjabloon- en tekstfragmenten ondersteunt u in het automatiseren van onderdelen van het schrijven van e-mails.
* **Meerdere talen.** Ingebouwde ondersteuning van vertalen van inhoud en per paragraaf spellingcontrole en ondersteuning van automatische detectie van talen bij het werken met inhoud in meerdere talen.
* **Productieve opsteller.** De opsteller van KMail komt met veel kleine hulpmodulen om u efficiënter te maken, zoals waarschuwen over vergeten bijlagen, compressie van inline bijlagen of van grootte wijzigen van bijgevoegde afbeeldingen.
* **Krachtige filtering.** Naast het automatisch sorteren van uw e-mails door een veelheid aan voorwaarden, kan het filtersysteem van KMail ook gebruikt worden voor het implementeren van willekeurige werkmethoden omdat het kan integreren met externe toepassingen.
* **Zoeken en tags toekennen.** Vlaggen en tags toekennen aan berichten helpt bij het sorteren en herstel van informatie en evenals de krachtige zoekfuncties lokaal en op afstand.
* **Beheer van e-maillijsten.** Mailman e-maillijsten kunnen automatisch gedetecteerd worden en algemene bewerkingen voor beheer van e-maillijsten zijn direct beschikbaar vanuit KMail.
* **Flexibele configuratie.** Ontelbare configuratie-opties bieden u het aanpassen van vele aspecten van KMail tot exact uw benodigdheden en wensen.

## Geïntegreerd

* **iTip invitaties.** E-mailuitnodigingen voor vergaderingen worden gedetecteerd en bieden ter plekke snelle antwoorden en worden natuurlijk ingevoerd in [KOrganizer](../korganizer).
* **Adresboek.** Naast automatische aanvulling van adressen uit [KAddressBook](../kaddressbook), gebruikt KMail daar uit ook avatars en voorkeuren voor versleuteling.
* **E-mails over boeken van reizen.** KMail kan vlucht- of hotelreserveringen detecteren in e-mails uit bepaalde leveranciers en biedt het toevoegen ervan aan uw agenda.

## Voldoet aan standaarden

* Ondersteunt de standaard e-mailprotocollen IMAP, POP3 en SMTP.
* Ondersteunt push e-mail (IMAP IDLE).
* Zoeken in IMAP-mappen wordt volledig ondersteund.
* Inheemse ondersteuning voor inline OpenPGP, PGP/MIME en S/MIME.
* Ondersteuning voor filteren met Sieve aan de kant van de server.
