---
description: 'KAddressBook biedt u het gemakkelijk beheren van uw contactpersonen.
  Het is diep geïntegreerd met de rest van Kontact en biedt u het gemakkelijk kiezen
  van uw contactpersonen bij het verzenden van e-mail of het aanmaken van een vergaderuitnodiging.

  '
layout: component
screenshot: /assets/img/kontact-kaddressbook.png
shortDescription: Uw contactpersonen gemakkelijk beheren.
title: KAddressBook
weight: 30
---
KAddressBook slaat alle persoonlijke details van uw familie, vrienden en andere contactpersonen. Het ondersteunt een grote variëteit van services, inclusief NextCloud, Kolab, Google Contacts, Microsoft Exchange (EWS) of elke standaard CalDAV-server.

## Mogelijkheden

* **Krachtig zoeken.** KAddresbook heeft te configureren filters en krachtige zoekmogelijkheden.
* **Detectie van duplicaten en samenvoegen.** - KAddressBook kan dubbele contacten vinden uit meerdere bronnen en kan ze samenvoegen in een enkel contact.
* **Integratie.** - Integreert met andere componenten van Kontact, bijv. exporteren van herinneringen bij verjaardagen naar [KOrganizer](../korganizer).
* **QR-codes.** - KAddressBook kan een QR-code tonen voor elk contactpersoon om het snel in te scannen met uw telefoon of naar iemand te verzenden.
* **Goede ondersteuning van standaarden.** - KAddressBook kan met bijna elke adresboekstandaard contacten importeren en exporteren.
* **LDAP-integratie.** - Kan verbinden naar meerdere LDAP-servers, die dan gebruikt kunnen worden voor automatisch voltooien van contactpersonen bij opstellen van e-mails in [KMail](../kmail).
