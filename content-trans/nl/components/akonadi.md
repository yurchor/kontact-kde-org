---
description: ''
layout: component
screenshot: /assets/img/akonadi.png
shortDescription: ''
title: Akonadi
weight: 70
---
Het framework Akonadi, genaamd naar de orakel godin van justitie in Ghana is verantwoordelijk voor het leveren van toepassingen met een gecentraliseerde database om de persoonlijke informatie van de gebruiker op te slaan, te indexeren en op te halen. Dit omvat de email-berichten van de gebruiker, contactpersonen, agenda's, afspraken, journalen, alarmen, notities, etc.

## Mogelijkheden

* Gezamenlijke gegevenscache van PIM
* Type agnostisch ontwerp
* Uitbreidbaarheid
* Generieke offline toegang, wijzig opnemen en afspelen
* Generieke conflictdetectie en oplossing
* Hulpbronnen zijn op profiel te groeperen
* Items samengesteld uit onafhankelijk op te halen meerdere delen
* Nul-kopie ophalen mogelijk
* Gelijktijdige toegang biedt achtergrond activiteit onafhankelijk van UI van client
* E-mail, agenda, adresboeken synchroniseren naar servers op afstand
* Biedt semantische infrastructuur op het bureaublad voor toegang tot PIM gegevens
* Archiveren
* Indexeren
* Zoeken buiten het proces om
* Ontwerp met meerdere processen
* Isolatie van crash
* Grote items kunnen niet het gehele systeem blokkeren
* Koppeling door IPC biedt componenten die eigendom zijn
