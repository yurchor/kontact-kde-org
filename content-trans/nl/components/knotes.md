---
description: ''
layout: component
shortDescription: ''
title: KNotes
weight: 50
---
Knotes is de digitale variant van de gele zelfklevende notitieblaadjes. De notities worden bij het afsluiten automatisch bewaard en worden weer getoond wanneer het programma gestart wordt.

## Mogelijkheden

* Schrijf notities met uw keuze voor lettertype en achtergrondkleur
* Slepen en neerzetten gebruiken om uw notities te e-mailen
* Kan versleept worden in Agenda om een time-slot te boeken
* Notities kunnen worden afgedrukt
