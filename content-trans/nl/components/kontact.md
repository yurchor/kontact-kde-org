---
description: 'Kontact voegt all uw PIM-toepassingen samen in een enkel venster om
  u de best mogelijke ervaring te geven en uw productiviteit te verhogen. Met Kontact
  kunt u alle hulpmiddelen gebruiken van uw krachtige suite op één plek. Anders dan
  webtoepassingen van derden waarderen we uw privacy en ondersteunen open standaarden,
  waarom wij in Kontact u volledige controle geven over uw gegevens. U kunt natuurlijk
  ook uw gegevens offline benaderen als u dat wilt.

  '
layout: component
screenshot: /assets/img/kontact-summary.png
shortDescription: 'Al uw e-mails, agenda''s en contactpersonen behandelen in een enkel
  venster.

  '
title: Kontact
weight: 10
---
Kontact is een suite, rijk aan mogelijkheden, voor persoonlijk informatiebeheer gemaakt door [KDE](https://kde.org). Kontact voegt al uw PIM-toepassingen samen in een enkel venster zodat u nog efficiënter kunt werken met uw e-mails, agenda's, nieuws en andere gegevens. U kunt elk van de componenten van Kontact ook als een alleenstaande toepassing gebruiken.

## Rijk aan mogelijkheden

We geloven dat software zich moet aanpassen aan uw werkwijze, in plaats van dat u zich aanpast aan hen. Kontact is zeer configureerbaar en aanpasbaar zodat u het kunt instellen op exact de manier die voor u werkt. Bekijk de individuele pagina's voor de componenten van Kontact om een overzicht te krijgen van sommige van de mogelijkheden die elk van hen kan bieden.

## Flexibel

Kontact ondersteunt een grote variëteit van e-mail-, agenda- en adresboekservices en -protocollen en kan gemakkelijk uitgebreid worden om additionele services te ondersteunen. U hoeft uw e-mailprovider niet te wijzigen of uw agenda niet ergens anders naar te migreren alleen om het in Kontact te kunnen synchroniseren. Laat Kontact wijzen naar de service waar uw PIM-gegevens zijn en Kontact zal ze tonen en gesynchroniseerd houden.

Kontact kan ook uw PIM-gegevens en sommige instellingen uit andere PIM-suites zoals Thunderbird en Evolution importeren.

## Is beveiligd

Kontact ondersteunt alle moderne beveiligingsstandaarden om uw privacy te beschermen. Alle communicatie met servers op afstand is standaard versleuteld met de industriestandaard SSL/TLS versleuteling. Kontact heeft ook een ingebouwde ondersteuning voor ondertekenen en versleutelen van e-mails met GPG en S/MIME en het kan automatisch ondertekeningen van e-mails verifiëren.

Kontact is standaard geconfigureerd om uw privacy te beschermen door het laden van externe inhoud, automatisch antwoorden en andere trucs te blokkeren die vaak gebruikt worden door scammers.

## Geïntegreerd

Alle componenten van Kontact zijn samen goed geïntegreerd. KOrganizer kan uitnodigingen voor bijeenkomsten verzenden via e-mailaccounts geconfigureerd in KMail. KMail zal uitnodigingen voor vergaderingen verwerken en u conflicten tonen met afspraken in uw agenda's. U kunt uw Tedoens en reguliere afspraken in de weergave van de agenda van KOrganizer mengen.

## Vrij en open-source

Kontact is vrij (zowel als in vrije spraak en vrij bier) en volledig open-source. Het betekent dat de [broncode](https://invent.kde.org/pim/kontact) er is voor iedereen te zien, te onderzoeken en te verbeteren.

We zijn trots op onze openheid en onze focus op het beschermen van de privacy van onze gebruikers. Kontact zal u niet bespioneren, geen enkele advertentie tonen, het zal uw gevoelige gegevens niet ergens opslaan in een cloud van derden of ze delen met iemand anders. Zie [KDE Software Privacy Policy](https://kde.org/privacypolicy-apps/) voor meer details.
