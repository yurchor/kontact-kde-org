---
appstream: org.kde.kontact
components: true
flatpak: true
flatpak_exp: https://community.kde.org/KDE_PIM/Flatpak
gear: true
konqi: /assets/img/konqi-mail.png
layout: download
menu:
  main:
    weight: 2
name: Kontact
sources:
- description: 'Kontact is verspreid over een aantal opslagruimten. Bekijk onze pagina
    [Mee doen](/get-involved) voor gedetailleerde instructies over hoe alle opslagruimten
    van Kontact te verkrijgen.

    '
  name: Git
  src_icon: /reusable-assets/git.svg
- description: 'We werken nu aan het brengen van Kontact naar Windows. Wilt u ons
    helpen? Bekijk [hoe mee te doen](/get-involved) en raak met ons in contact!

    '
  name: Vensters
  src_icon: /reusable-assets/windows.svg
title: Downloaden
---
