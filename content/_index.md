---
menu:
  main:
    name: Kontact
    weight: 1
    params:
      components:
      - name: Kontact Features
        url: /components/kontact
        large: true
        background: '/assets/img/kontact-menu-features.png'
      - name: KMail
        url: /components/kmail
        background: '/assets/img/icon-kmail.svg'
      - name: KOrganizer
        url: /components/korganizer
        background: '/assets/img/icon-korganizer.svg'
      - name: KAddressBook
        url: /components/kaddressbook
        background: '/assets/img/icon-kaddressbook.svg'
      - name: Akregator
        url: /components/akregator
        background: '/assets/img/icon-akregator.svg'
      - name: KNotes
        url: /components/knotes
        background: '/assets/img/icon-knotes.svg'
      - name: Akonadi
        url: /components/akonadi
        background: '/assets/img/icon-akonadi.png'
title: Kontact Suite - The Powerful PIM Solution
---
